package com.example.mvvmretrofit;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseModel {

    private List<ResponseModelData> mData;

    public List<ResponseModelData> getmData() {
        return mData;
    }

    public void setmData(List<ResponseModelData> mData) {
        this.mData = mData;
    }
}
