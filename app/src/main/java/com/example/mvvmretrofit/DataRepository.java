package com.example.mvvmretrofit;

import android.app.Application;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataRepository {

    private ArrayList<ResponseModelData> Data = new ArrayList<>();
    private MutableLiveData<List<ResponseModelData>> mutableLiveData = new MutableLiveData<>();
    private Application application;


    public DataRepository(Application application) {
        this.application = application;
    }

    public MutableLiveData<List<ResponseModelData>> getMutableLiveData() {

        ApiInterface apiService = RetrofitService.getApiService();

        Call<List<ResponseModel>> call = apiService.getPosts();

        call.enqueue(new Callback<List<ResponseModel>>() {
            @Override
            public void onResponse(Call<List<ResponseModel>> call, Response<List<ResponseModel>> response) {
                ResponseModel model  = (ResponseModel) response.body();
                if ( model.getmData() != null) {
                    Data = (ArrayList<ResponseModelData>) model.getmData();
                    mutableLiveData.setValue(Data);
            }

            }


            @Override
            public void onFailure(Call<List<ResponseModel>> call, Throwable t) {
            }
            });


        return mutableLiveData;


    }
}