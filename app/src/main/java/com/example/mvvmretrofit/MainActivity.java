package com.example.mvvmretrofit;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.res.Configuration;
import android.os.Bundle;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    private DataViewModel mainViewModel;
    Adapter mBlogAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainViewModel = ViewModelProviders.of(this).get(DataViewModel.class);
        getPosts();
        initializationViews();



    }
    private void initializationViews() {
        recyclerView = findViewById(R.id.recycler);
    }

    private void getPosts() {

        mainViewModel.getAllBlog().observe(this, new Observer<List<ResponseModelData>>() {
            @Override
            public void onChanged(List<ResponseModelData> responseModelData) {
                prepareRecyclerView(responseModelData);
            }
        });
    }

    private void prepareRecyclerView(List<ResponseModelData> responseModelData) {
        mBlogAdapter = new Adapter(responseModelData);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
        }
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mBlogAdapter);
        mBlogAdapter.notifyDataSetChanged();

        //change for bitbucket
    }
}
