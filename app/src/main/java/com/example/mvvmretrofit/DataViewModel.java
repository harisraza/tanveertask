package com.example.mvvmretrofit;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

public class DataViewModel extends AndroidViewModel {

    private DataRepository movieRepository;

    public DataViewModel(@NonNull Application application) {
        super(application);
        movieRepository = new DataRepository(application);
    }

    public LiveData<List<ResponseModelData>> getAllBlog() {
        return movieRepository.getMutableLiveData();
    }


}
