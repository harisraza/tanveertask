package com.example.mvvmretrofit;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {

    Context context;
    private List<ResponseModelData> mBlogList;

    public Adapter(List<ResponseModelData> blogList) {
        mBlogList = blogList;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitems, parent, false);

        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {

        ResponseModelData model = mBlogList.get(i);
        holder.text_id.setText(mBlogList.get(i).getId());
        holder.text_userid.setText(mBlogList.get(i).getUserId());
        holder.text_title.setText(mBlogList.get(i).getTitle());
        holder.text_body.setText(mBlogList.get(i).getBody());


    }

    @Override
    public int getItemCount() {
        return mBlogList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView text_id,text_userid,text_title,text_body;
        public MyViewHolder(View itemView) {
            super(itemView);
            text_id=itemView.findViewById(R.id.id);
            text_userid=itemView.findViewById(R.id.user_d);
            text_title=itemView.findViewById(R.id.title_body);
            text_body = itemView.findViewWithTag(R.id.body);

        }
    }
}
